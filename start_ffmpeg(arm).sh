#!/bin/bash

COUNT='2'

int_TERM ()
{

        pkill -TERM -P "$$" &>> /dev/null
        exit 0

}

trap int_TERM 2 15 20

ffmpeg -rtsp_transport tcp -reorder_queue_size 1400000 -i 'rtsp://192.168.50.106:554/ch0_0.h264' -f v4l2 /dev/video5 &>> /dev/null &
ffmpeg_1_pid=$!
echo "PID: $ffmpeg_1_pid"

while [ 1 ]
do

        sleep 10

        ping -c $COUNT 192.168.50.106 >/dev/null 2>&1
        CODE_PING=$?

        if [ $(ps "$ffmpeg_1_pid" | wc -l) -eq '1' ] || [ $CODE_PING -ne '0' ]
        then
                pkill -TERM -P "$$" &>> /dev/null
                ffmpeg -rtsp_transport tcp -reorder_queue_size 1400000 -i 'rtsp://192.168.50.106:554/ch0_0.h264' -f v4l2 /dev/video5 &>> /dev/null &
                ffmpeg_1_pid=$!
               echo "RESET. NEW PID: $ffmpeg_1_pid"
        fi

done

