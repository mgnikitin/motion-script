#!/bin/bash

if [ `du -BG -s /opt/cam-video | awk '{print $1}' | sed 's/G//'` -gt 6 ]
then
#echo "DEL $(find "/media/DATA0/cam-video/cam1" | grep "mkv$" | sort | sed -n '1p')"
#rm $(find "/media/DATA0/cam-video/cam1" | grep "mkv$" | sort | sed -n '1p')
#echo "DEL $(find "/opt/cam-video/cam2" | grep "mkv$" | sort | sed -n '1p')"
rm $(find "/opt/cam-video/cam2" | grep "mkv$" | sort | sed -n '1p')
fi
